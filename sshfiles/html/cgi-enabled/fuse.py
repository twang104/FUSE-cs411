from flask import Flask
#app=Flask(__name__)
from flask import Response, render_template, redirect,url_for, request
from flaskext.mysql import MySQL
from flask_bootstrap import Bootstrap
from passlib.hash import sha256_crypt

app=Flask(__name__)
app.debug=True
Bootstrap(app)


app.config['MYSQL_HOST']='localhost'
app.config['MYSQL_USER']='root'
app.config['MYSQL_PASSWORD']='zd642142'
app.config['MYSQL_DB']='FUSE'
app.config['MYSQL_CURSORCLASS']='DictCursor'


mysql = MySQL(app)





@app.route('/')
def index():
    return render_template("index.html")


@app.route('/register',methods=['GET','POST'])
def register():
    if request.method=='POST':
        username=request.form['username']
        password=sha256_crypt.encrypt(str(request.form['password']))

        cur=mysql.connection.cursor()
        cur.execute("INSERT INTO user(username, password) VALUES(%s,%s)",(username,password))

        mysql.connection.commit()
        cur.close()

        redirect(url_for('index'))

    return render_template('register.html')




if __name__ == '__main__':
    app.run()
