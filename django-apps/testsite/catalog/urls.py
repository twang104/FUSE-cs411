from django.urls import path
from . import views


urlpatterns = [
    path('catalog/', views.createEvent, name='add_new_event'),
    path('listing/', views.showlisting, name='dashboard-listing-table'),
    path('delete/<id>/', views.deleteevent, name='delete'),
]

