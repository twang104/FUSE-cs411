from django import forms



class NewEventForm(forms.Form):
    title = forms.CharField(max_length=100)
    keyword = forms.CharField(max_length=100)
    address = forms.CharField(max_length=100)
    description = forms.CharField()