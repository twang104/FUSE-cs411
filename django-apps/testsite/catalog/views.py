from django.shortcuts import render, get_object_or_404
from catalog.models import Event
from catalog.forms import NewEventForm

# Create your views here.
from django.http import HttpResponse
from django.http import HttpResponseRedirect


def showlisting(request):
    events_db = Event.objects.all()
    return render(
        request,
        'dashboard-listing-table.html',
        context = {'events':events_db,}
    )

def deleteevent(request,id=None):
    delete_evt = get_object_or_404(Event,id=id)
    delete_evt.delete()
    return HttpResponseRedirect("/listing/")



def createEvent(request):
    if request.method == 'POST':
        form = NewEventForm(request.POST)
        new_title = request.POST.get('title_fd','')
        new_addr = request.POST.get('addr_fd','')
        new_keywd = request.POST.get('keywd_fd','')
        new_desc = request.POST.get('desc_fd','')
        new_event_obj = Event(title=new_title,keyword=new_keywd,address=new_addr,description=new_desc)
        new_event_obj.save()
        return render(
        request,
        'add_new_event.html'
    )

    else:
        return render(
        request,
        'add_new_event.html'
    )

