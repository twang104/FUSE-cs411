from django.contrib import admin
from catalog.models import Post
from catalog.models import Comment
from catalog.models import Event
from catalog.models import UserProfile

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Event)
admin.site.register(UserProfile)
